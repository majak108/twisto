# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'twisto/version'

Gem::Specification.new do |spec|
  spec.name          = "twisto"
  spec.version       = Twisto::VERSION
  spec.authors       = ["Mixit"]
  spec.email         = ["kuba@mixit.cz"]

  spec.summary       = %q{Ruby client library for Twisto Payments API.}
  spec.description   = %q{Ruby client library for Twisto Payments API.}
  spec.homepage      = "https://bitbucket.org/majak108/twisto"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://bitbucket.org/majak108/twisto"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_development_dependency "minitest", "~> 5.4.2"
  spec.add_development_dependency "vcr", "~> 2.9.3"
  spec.add_development_dependency "webmock", "~> 1.19.0"
end
