require 'twisto/short_address'

module Twisto
  class Invoice

    attr_accessor :twisto
    attr_accessor :invoice_id
    attr_accessor :eshop_invoice_id
    attr_accessor :customer_email
    attr_accessor :billing_address
    attr_accessor :delivery_address
    attr_accessor :date_created
    attr_accessor :date_returned
    attr_accessor :date_returned
    attr_accessor :date_cancelled
    attr_accessor :date_activated
    attr_accessor :date_paid
    attr_accessor :pdf_url
    attr_accessor :total_price_vat
    attr_accessor :items

    def initialize(twisto, invoice_id)
      self.twisto = twisto
      self.invoice_id = invoice_id
    end

    def get

    end

    def deserialize(data)
      self.invoice_id = data['invoice_id']
      self.eshop_invoice_id = data['eshop_invoice_id']
      customer_email = data['customer_email']
      %w{ billing_address delivery_address }.each do |a|
        if data[a]
          if data[a]['type'] == ::Twisto::BaseAddress::TYPE_SHORT
            self.__send__ "#{a}=", ::Twisto::ShortAddress.deserialize(data[a])
          else
            self.__send__ "#{a}=", ::Twisto::Address.deserialize(data[a])
          end
        end
      end
      %w{ date_created date_returned date_cancelled date_activated }.each do |d|
        if data[d]
          __send__ "#{d}=", Time.parse(data[d])
        end
      end
      self.pdf_url = data['pdf_url']
      self.total_price_vat = data['total_price_vat'].to_f
      self.items = data['items'].map { |i| Twisto::Item.deserialize(i) } if data['items']
      self
    end

  end
end
