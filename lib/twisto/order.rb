module Twisto
  class Order

    attr_accessor :date_created
    attr_accessor :billing_address
    attr_accessor :delivery_address
    attr_accessor :total_price_vat
    attr_accessor :items

    def initialize(date_created, billing_address, delivery_address, total_price_vat, items)
      self.date_created = date_created
      self.billing_address = billing_address
      self.delivery_address = delivery_address
      self.total_price_vat = total_price_vat
      self.items = items
    end

    def serialize
      {
        date_created: self.date_created,
        billing_address: self.billing_address.serialize,
        delivery_address: self.delivery_address.serialize,
        total_price_vat: self.total_price_vat,
        items: self.items.map(&:serialize),
      }
    end

  end
end
