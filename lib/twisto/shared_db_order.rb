module Twisto
  class SharedDBOrder

    attr_accessor :order_id
    attr_accessor :date_created
    attr_accessor :total_price_vat

    def initialize(order_id, date_created, total_price_vat)
      unless date_created.is_a? Time
        raise 'Date created is not Time object.'
      end

      self.order_id = order_id
      self.date_created = date_created
      self.total_price_vat = total_price_vat
    end

    def serialize
      {
        order_id: self.order_id,
        date_created: self.date_created.iso8601,
        approx_total_price: (self.total_price_vat.to_f <= 500.0 ? 1 : 2)
      }
    end

  end
end
