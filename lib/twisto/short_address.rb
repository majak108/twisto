require 'twisto/address'

module Twisto
  class ShortAddress < BaseAddress

    attr_accessor :name
    attr_accessor :phone_number
    attr_accessor :country

    def initialize(name, country, phone_number)
      self.name = name
      self.country = country
      self.phone_number = phone_number
    end

    def serialize
      {
        type: TYPE_SHORT,
        name: self.name,
        country: self.country,
        phone_number: self.phone_number
      }
    end

    def self.deserialize(data)
      ShortAddress.new(data['name'], data['country'], data['phone_number'])
    end

  end
end
