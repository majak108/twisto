module Twisto
  class SharedDBCustomer

    require 'digest'

    attr_accessor :email
    attr_accessor :facebook_id

    def initialize(email, facebook_id)
      self.email = email
      self.facebook_id = facebook_id
    end

    def serialize
      data = {}
      data[:email_hash] = Digest::MD5.hexdigest(self.email.to_lower_case.strip)

      if self.facebook_id && !self.facebook_id.empty?
        data[:facebook_id_hash] = Digest::MD5.hexdigest(self.facebook_id)
      end

      data
    end

  end
end
