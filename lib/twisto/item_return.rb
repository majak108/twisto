module Twisto
  class ItemReturn

    attr_accessor :product_id
    attr_accessor :quantity

    # Initialize object and sets item attributes.
    def initialize(product_id, quantity)
      self.product_id = product_id
      self.quantity = quantity
    end

    # Serialize data as Hash.
    def serialize
      {
        product_id: self.product_id,
        quantity: self.quantity
      }
    end

  end
end
