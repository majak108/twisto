module Twisto
  class Customer

    attr_accessor :email
    attr_accessor :name
    attr_accessor :facebook_id
    attr_accessor :company_id
    attr_accessor :vat_id

    # Initialize object and sets customer attributes.
    def initialize(email, name, facebook_id, company_id, vat_id)
      self.email = email
      self.name = name
      self.facebook_id = facebook_id
      self.company_id = company_id
      self.vat_id = vat_id
    end

    # Serialize data as Hash.
    def serialize
      {
        email: self.email,
        name: self.name,
        facebook_id: self.facebook_id,
        company_id: self.company_id,
        vat_id: self.vat_id
      }
    end

  end
end
