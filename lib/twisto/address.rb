require 'twisto/base_address'

module Twisto
  class Address < BaseAddress

    attr_accessor :name
    attr_accessor :street
    attr_accessor :city
    attr_accessor :zipcode
    attr_accessor :phone_number
    attr_accessor :country

    # Initialize object and sets address attributes.
    def initialize(name, street, city, zipcode, country, phone_number)
      self.name = name
      self.street = street
      self.city = city
      self.zipcode = zipcode
      self.country = country
      self.phone_number = phone_number
    end

  end
end
