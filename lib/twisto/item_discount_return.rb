module Twisto
  class ItemDiscountReturn

    attr_accessor :product_id
    attr_accessor :price_vat

    def initialize(product_id, price_vat)
      self.product_id = product_id
      self.price_vat = price_vat
    end

  end
end
