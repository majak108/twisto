require 'net/http'
require 'multi_json'
require 'twisto/version'
require 'twisto/base'
require 'twisto/error'
require 'twisto/address'
require 'twisto/invoice'
require 'twisto/request_error'
require 'twisto/server_error'
require 'securerandom'

module Twisto
  class Client < Twisto::Base

    attr_reader :public_key, :private_key, :api_url

    DEFAULTS = {
      api_url: 'https://api.twisto.cz/v2/',
      use_ssl: true
    }

    HTTP_HEADERS = {
      'Content-Type'    => 'application/json',
      'Accept'          => 'application/json',
      'Accept-Charset'  => 'utf-8',
      'User-Agent'      => "twisto-ruby-client/#{::Twisto::VERSION} ruby/#{RUBY_VERSION}"
    }


    def initialize(options = {})
      @options = DEFAULTS.merge options
      @api_url = URI.parse @options[:api_url]
      raise ArgumentError.new("The :public_key needs to be specified.") unless @options[:public_key]
      raise ArgumentError.new("The :private_key needs to be specified.") unless @options[:private_key]
      @public_key = @options[:public_key]
      @private_key = @options[:private_key]
    end

    # Create check payload
    #
    # Parameters:
    #
    # customer
    # order
    # previous_orders
    #
    def get_check_payload(customer, order, previous_orders)
      encrypt({
        random_nonce: SecureRandom.hex,
        customer: customer.serialize,
        order: order.serialize,
        previous_orders: previous_orders.map(&:serialize)
      })
    end

    # Create new invoice using transaction_id from check
    #
    # Parameters:
    #
    # transaction_id
    # eshop_invoice_id
    #
    def create_invoice(transaction_id, eshop_invoice_id = nil)
      data = { transaction_id: transaction_id }
      if eshop_invoice_id
        data[:eshop_invoice_id] = eshop_invoice_id
      end
      response = request_json('POST', 'invoice/', data)
      invoice = Twisto::Invoice.new(self, nil)
      invoice.deserialize(response)
      invoice
    end

    # Activate invoice using transaction_id from check
    #
    # Parameters:
    #
    # invoice
    #
    def activate_invoice(invoice)
      response = request_json('POST', "invoice/#{invoice.invoice_id}/activate/")
      invoice.deserialize(response)
      invoice
    end

    def request_json(http_method, path, data = nil)
      method_klass = Net::HTTP.const_get http_method.to_s.capitalize
      request_path = "#{@api_url.path}#{path}"
      headers = HTTP_HEADERS.merge({
        'Authorization' => "#{@options[:public_key]},#{@options[:private_key]}",
      })
      request = method_klass.new request_path, headers
      request.body = data.is_a?(Hash) ? MultiJson.dump(data) : data
      send_request request
    end

    def send_request(request)
      http = Net::HTTP.new(@api_url.host, @api_url.port)
      http.open_timeout = @options[:open_timeout]
      http.read_timeout = @options[:read_timeout]
      http.use_ssl = @options[:use_ssl]
      response = http.request(request)
      if response.body and !response.body.empty?
        object = MultiJson.load response.body
      else
        object = true
      end
      if response.kind_of? Net::HTTPClientError
        raise Twisto::RequestError.new(response.code), (object['detail'] || response.body)
      elsif response.kind_of? Net::HTTPServerError
        raise Twisto::ServerError.new(response.code), (object['detail'] || response.body)
      end
      object
    end

    def javascript_library_html_code
      "<script type=\"text/javascript\">
          var _twisto_config = {
              public_key: '#{@public_key}',
              script: 'https://static.twisto.cz/api/v2/twisto.js'
          };
          (function(e,g,a){function h(a){return function(){b._.push([a,arguments])}}var f=[\"check\"],b=e||{},c=document.createElement(a);a=document.getElementsByTagName(a)[0];b._=[];for(var d=0;d<f.length;d++)b[f[d]]=h(f[d]);this[g]=b;c.type=\"text/javascript\";c.async=!0;c.src=e.script;a.parentNode.insertBefore(c,a);delete e.script}).call(window,_twisto_config,\"Twisto\",\"script\");
      </script>"
    end

  end
end
