require 'openssl'
require 'zlib'
require 'json'
require 'base64'

require 'twisto/order'
require 'twisto/address'
require 'twisto/customer'
require 'twisto/item'

module Twisto
  class Base

    # Encrypts data by private key.
    def encrypt(data)
      # remove _live_sk_
      key = self.private_key[8..-1]

      # convert hexadecimal representation to binary data
      encryption_key = key.scan(/../).map { |x| x.hex.chr }.join

      cipher = OpenSSL::Cipher::Cipher.new('AES-128-CBC')

      # generate random initialization vector
      iv = cipher.random_iv

      # use first 16 bytes as encryption key, remaining 16 as a salt
      key = encryption_key[0...16]
      salt = encryption_key[16...encryption_key.length]

      # sets up cipher with generated key and iv
      cipher.encrypt
      cipher.key = key
      cipher.iv = iv

      # sets up cipher with generated key and iv
      serialized_data = data.to_json.encode('utf-8')

      # compress data
      serialized_data = Zlib::Deflate.deflate(serialized_data, Zlib::BEST_COMPRESSION)

      # add payload length as unsigned long (big endian - network byte order) to the beginning of binary data
      serialized_data = [serialized_data.length].pack("L").unpack('N*').pack('V*') + serialized_data

      # compute digest
      digest = OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), salt, serialized_data + iv)

      # Backward compatibility with Ruby < 2.0.
      # In Ruby < 2.0 method bytes returns Enumerator, while in higher version it returns Array.
      if RUBY_VERSION < '2.0'
        bytes = serialized_data.bytes.to_a
      else
        bytes = serialized_data.bytes
      end

      # add padding to the end of binary data to match AES block size
      bytes += Array.new([0, 16 - bytes.length % 16].max, 0)

      # encrypt data using AES
      encrypted_data = cipher.update(bytes.pack('c*'))

      # create base64 encoded string from concatenated IV, digest and encrypted data
      Base64.strict_encode64(iv + digest + encrypted_data).encode('utf-8')
    end

  end
end
