module Twisto
  class BaseAddress

    TYPE_FULL = 1
    TYPE_SHORT = 1

    # Serialize data as Hash.
    def serialize
      {
        name: self.name,
        street: self.street,
        city: self.city,
        zipcode: self.zipcode,
        country: self.country,
        phone_number: self.phone_number,
      }
    end

    # Deserialize data from Hash.
    def self.deserialize(data)
      if data.is_a? Hash
        return Address.new(data['name'], data['street'], data['city'], data['zipcode'], data['country'], data['phone_number'])
      else
        raise 'Not a Hash object.'
      end
    end

  end
end
