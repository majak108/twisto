module Twisto
  class Error < StandardError

    attr_accessor :data

    def initialize(message, data, code, previous)
      self.data = data
    end

  end
end
