module Twisto
  class RequestError < ::Twisto::Error
    attr_reader :code
    def initialize(code)
      @code = code
    end
  end
end
