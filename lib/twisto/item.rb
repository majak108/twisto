module Twisto
  class Item

    TYPE_DEFAULT = 0
    TYPE_SHIPMENT = 1
    TYPE_PAYMENT = 2
    TYPE_DISCOUNT = 4
    TYPE_ROUND = 32

    attr_accessor :type
    attr_accessor :name
    attr_accessor :product_id
    attr_accessor :quantity
    attr_accessor :price_vat
    attr_accessor :vat
    attr_accessor :ean_code
    attr_accessor :isbn_code
    attr_accessor :issn_code
    attr_accessor :heureka_category

    # Initialize object and sets item attributes.
    def initialize(type, name, product_id, quantity, price_vat, vat, ean_code = nil, isbn_code = nil, issn_code = nil, heureka_category = nil)
      self.type = type
      self.name = name
      self.product_id = product_id
      self.quantity = quantity
      self.price_vat = price_vat
      self.vat = vat
      self.ean_code = ean_code
      self.isbn_code = isbn_code
      self.issn_code = issn_code
      self.heureka_category = heureka_category
    end

    # Serialize data as Hash.
    def serialize
      {
        type: self.type,
        name: self.name,
        product_id: self.product_id,
        quantity: self.quantity,
        price_vat: self.price_vat,
        vat: self.vat,
        ean_code: self.ean_code,
        isbn_code: self.isbn_code,
        issn_code: self.issn_code,
        heureka_category: self.heureka_category
      }
    end

    # Deserialize data from Hash.
    def self.deserialize(data)
      if data.is_a? Hash
        return Item.new(data[:type], data[:name], data[:product_id], data[:quantity], data[:price_vat], data[:vat], data[:ean_code], data[:isbn_code], data[:issn_code], data[:heureka_category])
      else
        raise 'Not a Hash object.'
      end
    end

  end
end
