# About

This repository contains the ruby client library for Twisto Payments API.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'twisto'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install twisto

and then require it:

```ruby
require 'twisto'
```

## Usage

```ruby
# instantiate client
@client = Twisto::Client.new(
  public_key:  'test_pk_abcd',
  private_key: 'test_sk_abcdefg'
)

# create invoice
transaction_id = 123
eshop_invoice_id = 123456
invoice = @client.create_invoice(transaction_id, eshop_invoice_id)
```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request