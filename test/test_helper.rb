require 'minitest/autorun'
require 'minitest/pride'
require 'webmock/minitest'
require 'vcr'
require 'pp'

VCR.configure do |c|
  c.cassette_library_dir = "test/fixtures"
  c.hook_into :webmock
  c.default_cassette_options = {
    match_requests_on:  [ :method, :path ]
  }
end

require File.expand_path('../../lib/twisto.rb', __FILE__)