require_relative '../../test_helper'

class TestClient < Minitest::Test
  def setup
    @client = Twisto::Client.new(
      public_key:  'test_pk_a79398b972b4731288f3aca1fd8cc70dc2ec336b656a4d9187',
      private_key: 'test_sk_49cf817addbfa5981aea23771bbc843c1f0b82512be75135e7d2bb4577c88783'
    )

    @order_items = [
      Twisto::Item.new(
        Twisto::Item::TYPE_DEFAULT,     # type
        'Coca Cola 1 litr',             # name
        530,                            # product_id (ID produktu - unique per order)
        6,                              # quantity
        156,                            # price_vat (total price)
        15,                             # vat
        '5449000017888',                # ean_code (barcode)
        nil,                            # isbn_code
        nil,                            # issn_code
        3808                            # heureka_category (ID of Heureka category)
      )
    ]

    billing_address = Twisto::Address.new(
      'Jan Novák', 'Milady Horákové 116/109', 'Praha 6', '16000', 'CZ', '+420603604605'
    )
    delivery_address = Twisto::Address.new(
      'Jan Novák, Twisto Payments a.s.', 'Sokolovská 47/73', 'Praha 8', '18600', 'CZ', '+420603604605'
    )

    # Instance of NewOrder - containts all information about order
    @order = Twisto::Order.new(
      Time.now,                                        # date_created
      billing_address,
      delivery_address,
      @order_items.map { |i| i.price_vat }.inject(:+), # total_price_vat
      @order_items                                     # items
    );
  end

  def test_javascript_library_html_code
    assert @client.javascript_library_html_code.include?(@client.public_key)
  end

  def test_get_check_payload
    customer = Twisto::Customer.new(
      'novak@example.cz', 'Jan Novák', '1146217671', '01615165', 'CZ01615165'
    );

    # create payload to send to Twisto using javascript
    payload = @client.get_check_payload(customer, @order, [])

    assert payload
  end

  def test_create_invoice
    VCR.use_cassette 'create_invoice' do
      transaction_id = 'k1vi23jvndb07z3dzoausike'
      eshop_invoice_id = 123456
      invoice = @client.create_invoice(transaction_id, eshop_invoice_id)

      assert_equal '43100187', invoice.invoice_id
      assert_equal 'CZ', invoice.billing_address.country
    end
  end

end
